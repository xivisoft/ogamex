<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::redirect('/', '/login', 301);

// Overview
Route::get('/overview', 'OverviewController@index')->name('overview.index');

// Resources
Route::get('/resources', 'ResourcesController@index')->name('resources.index');
Route::get('/resources/settings', 'ResourcesController@settings')->name('resources.settings');
Route::post('/resources/settings', 'ResourcesController@settingsUpdate')->name('resources.settingsUpdate');
Route::get('/ajax/resources', 'ResourcesController@ajax')->name('resources.ajax');
Route::post('/resources/add-buildrequest', 'ResourcesController@addBuildRequest')->name('resources.addbuildrequest');
Route::post('/resources/cancel-buildrequest', 'ResourcesController@cancelBuildRequest')->name('resources.cancelbuildrequest');

// Facilities
Route::get('/facilities', 'FacilitiesController@index')->name('facilities.index');
Route::get('/ajax/facilities', 'FacilitiesController@ajax')->name('facilities.ajax');
Route::post('/facilities/add-buildrequest', 'FacilitiesController@addBuildRequest')->name('facilities.addbuildrequest');
Route::post('/facilities/cancel-buildrequest', 'FacilitiesController@cancelBuildRequest')->name('facilities.cancelbuildrequest');

// Research
Route::get('/research', 'ResearchController@index')->name('research.index');
Route::get('/ajax/research', 'ResearchController@ajax')->name('research.ajax');
Route::post('/research/add-buildrequest', 'ResearchController@addBuildRequest')->name('research.addbuildrequest');
Route::post('/research/cancel-buildrequest', 'ResearchController@cancelBuildRequest')->name('research.cancelbuildrequest');

// Shipyard
Route::get('/shipyard', 'ShipyardController@index')->name('shipyard.index');
Route::get('/ajax/shipyard', 'ShipyardController@ajax')->name('shipyard.ajax');
Route::post('/shipyard/add-buildrequest', 'ShipyardController@addBuildRequest')->name('shipyard.addbuildrequest');


// Defense
Route::get('/defense', 'DefenseController@index')->name('defense.index');
Route::get('/ajax/defense', 'DefenseController@ajax')->name('defense.ajax');
Route::post('/defense/add-buildrequest', 'DefenseController@addBuildRequest')->name('defense.addbuildrequest');


// Misc
Route::get('/merchant', 'MerchantController@index')->name('merchant.index');

Route::get('/fleet', 'FleetController@index')->name('fleet.index');
Route::get('/galaxy', 'GalaxyController@index')->name('galaxy.index');
Route::get('/alliance', 'AllianceController@index')->name('alliance.index');
Route::get('/premium', 'PremiumController@index')->name('premium.index');
Route::get('/shop', 'ShopController@index')->name('shop.index');

Route::get('/options', 'OptionsController@index')->name('options.index');
Route::post('/options', 'OptionsController@save')->name('options.save');

Auth::routes();
