<?php

namespace OGame\Services;

use Illuminate\Support\Facades\DB;
use OGame\Planet as Planet;
use OGame\Services\PlanetService;

/**
 * Class PlanetList.
 *
 * Wrapper object which can contain one or more Planet objects.
 *
 * @package OGame\Services
 */
class PlanetListService
{

    /**
     * The planet object from the model.
     *
     * @var
     */
    protected $planets = [];

    /**
     * Planets constructor.
     */
    public function __construct() {

    }

    /**
     * Load all planets of specific user.
     */
    public function load($id) {
        // Get all planets of user
        $planets = Planet::where('user_id', $id)->get();
        foreach ($planets as $record) {
            $planet = resolve('OGame\Services\PlanetService');
            $planet->loadByPlanetId($record->id);

            $this->planets[] = $planet;
        }

        // If no planets, create at least one.
        if (empty($this->planets)) {
            $planet = resolve('OGame\Services\PlanetService');
            $planet->create($id);

            $this->planets[] = $planet;
        }
    }

    /**
     * Updates all planets in this planet list.
     */
    public function update() {
        foreach ($this->planets as $planet) {
            $planet->update();
        }
    }

    /**
     * Get already loaded child planet by ID. Invokes an exception if the
     * planet is not found.
     */
    public function childPlanetById($id) {
        foreach ($this->planets as $planet) {
            if ($planet->getPlanetId() == $id) {
                return $planet;
            }
        }

        throw new \Exception('Requested planet is not owned by this player.');
    }

    /**
     * Get first planet of player.
     */
    public function first() {
        return $this->planets[0];
    }

    /**
     * Get current planet of player.
     */
    public function current() {
        return $this->first(); // @TODO: change logic to real current planet.
    }

    /**
     * Return array of planet objects.
     */
    public function all() {
        return $this->planets;
    }

    /**
     * Get amount of planets.
     */
    public function count() {
        return count($this->planets);
    }
}
