<?php

namespace OGame\Http\Controllers;

use Illuminate\Http\Request;
use OGame\Http\Traits\IngameTrait;
use OGame\Services\PlayerService;

class OverviewController extends Controller
{
  use IngameTrait;

  protected $player;

  /**
   * Shows the overview index page
   *
   * @param  int  $id
   * @return Response
   */
  public function index(Request $request, PlayerService $player)
  {
    $this->player = $player;

    return view('ingame.overview.index')->with([
      'planet_name' => $this->player->planets->first()->getPlanetName(),
      'planet_diameter' => $this->player->planets->first()->getPlanetDiameter(),
      'planet_temp_min' => $this->player->planets->first()->getPlanetTempMin(),
      'planet_temp_max' => $this->player->planets->first()->getPlanetTempMax(),
      'planet_coordinates' => $this->player->planets->first()->getPlanetCoordinatesAsString(),
      'user_points' => 0, // @TODO
      'user_rank' => 0, // @TODO
      'max_rank' => 0, // @TODO
      'user_honor_points' => 0, // @TODO
      'body_id' => 'overview', // Sets <body> tag ID property.
    ]);
  }
}
