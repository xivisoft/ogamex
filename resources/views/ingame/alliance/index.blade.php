@extends('ingame.layouts.main')

@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div id="eventboxContent" style="display: none">
        <img height="16" width="16" src="https://gf3.geo.gfsrv.net/cdne3/3f9884806436537bdec305aa26fc60.gif">
    </div>

    <div id="netz">
        <div id="alliance">
            <div id="inhalt">

                <div id="planet" class="">
                    <h2>Alliance</h2>
                    <a class="toggleHeader" href="javascript:void(0);">
                        <img alt="" src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" height="22" width="22">
                    </a>
                </div>
                <div class="c-left"></div>
                <div class="c-right"></div>


                <div id="tabs">
                    <ul class="tabsbelow" id="tab-ally">
                        <li class="aktiv">
                            <a href="javascript:void(0);">
                                <span>Create alliance</span>
                            </a>
                        </li>

                        <li>
                            <a class="overlay" href="https://s144-en.ogame.gameforge.com/game/index.php?page=search&amp;category=4&amp;ajax=1">
            <span>
                Search alliance                </span>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="clearfloat"></div>
                <div id="eins">
                    <div id="drei">
                        <form action="" id="form_createAlly" method="post" name="asdf" class="formValidation">
                            <div class="sectioncontent" id="section11" style="display:block;">
                                <div class="contentz">
                                    <table class="createnote createALLY">
                                        <tbody><tr>
                                            <td class="desc">
                                                Alliance-Tag (3-8 characters):
                                            </td>
                                            <td class="value">
                                                <input class="text w200 validate[optional,custom[noSpecialCharacters],custom[noBeginOrEndUnderscore],custom[noBeginOrEndWhitespace],custom[noBeginOrEndHyphen],custom[notMoreThanThreeUnderscores],custom[notMoreThanThreeWhitespaces],custom[notMoreThanThreeHyphen],custom[noCollocateUnderscores],custom[noCollocateWhitespaces],custom[noCollocateHyphen],minSize[3]]" style="padding:3px;" type="text" size="8" name="allyTag" id="allyTagField" maxlength="8" value="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="desc">
                                                Alliance-Name (3-30 characters):
                                            </td>
                                            <td class="value">
                                                <input class="text w200 validate[optional,custom[noSpecialCharacters],custom[noBeginOrEndUnderscore],custom[noBeginOrEndWhitespace],custom[noBeginOrEndHyphen],custom[notMoreThanThreeUnderscores],custom[notMoreThanThreeWhitespaces],custom[notMoreThanThreeHyphen],custom[noCollocateUnderscores],custom[noCollocateWhitespaces],custom[noCollocateHyphen],minSize[3]]" style="padding:3px;" type="text" size="30" name="allyName" id="allyNameField" maxlength="30" value="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="center">

                                                <a href="javascript:void(0);" rev="form_createAlly" rel="16" class=" action btn_blue">
                                                    Create alliance                            </a>

                                            </td>
                                        </tr>
                                        </tbody></table>
                                </div><!--contentdiv -->
                                <div class="footer"></div>
                            </div><!-- section11 -->
                        </form>
                    </div>

                    <script type="text/javascript">
                        (function($){$.fn.validationEngineLanguage=function(){};$.validationEngineLanguage={newLang:function(){$.validationEngineLanguage.allRules={"minSize":{"regex":"none","alertText":"Not enough characters"},"pwMinSize":{"regex":/^.{4,}$/,"alertText":"The entered password is to short (min. 4 characters)"},"pwMaxSize":{"regex":/^.{0,20}$/,"alertText":"The entered password is to long (max. 20 characters)"},"email":{"regex":/^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/,"alertText":"You need to enter a valid email address!"},"noSpecialCharacters":{"regex":/^[a-zA-Z0-9\-_\s]+$/,"alertText":"Contains invalid characters."},"noBeginOrEndUnderscore":{"regex":/^([^_]+(.*[^_])?)?$/,"alertText":"Your name may not start or end with an underscore."},"noBeginOrEndHyphen":{"regex":/^([^\-]+(.*[^\-])?)?$/,"alertText":"Your name may not start or finish with a hyphen."},"noBeginOrEndWhitespace":{"regex":/^([^\s]+(.*[^\s])?)?$/,"alertText":"Your name may not start or end with a space."},"notMoreThanThreeUnderscores":{"regex":/^[^_]*(_[^_]*){0,3}$/,"alertText":"Your name may not contain more than 3 underscores in total."},"notMoreThanThreeHyphen":{"regex":/^[^\-]*(\-[^\-]*){0,3}$/,"alertText":"Your name may not contain more than 3 hyphens."},"notMoreThanThreeWhitespaces":{"regex":/^[^\s]*(\s[^\s]*){0,3}$/,"alertText":"Your name may not include more than 3 spaces in total."},"noCollocateUnderscores":{"regex":/^[^_]*(_[^_]+)*_?$/,"alertText":"You may not use two or more underscores one after the other."},"noCollocateHyphen":{"regex":/^[^\-]*(\-[^\-]+)*-?$/,"alertText":"You may not use two or more hyphens consecutively."},"noCollocateWhitespaces":{"regex":/^[^\s]*(\s[^\s]+)*\s?$/,"alertText":"You may not use two or more spaces one after the other."}}}}
                            $.validationEngineLanguage.newLang();})(jQuery);var defaultName="duh";$('select').ogameDropDown();$('.action').click(function(){var action=$(this).attr('rel');var form=$(this).attr('rev');var reload=$(this).attr('data-reload');var token=$(this).attr('token');var url='https://s144-en.ogame.gameforge.com/game/index.php?page=allianceCreation&action='+action;$.post(url,$("#"+form).serialize()+'&token='+token,function(data){if(reload){window.location.href=getRedirectLink();}else{$('#eins').html(data);}})});$("form").each(function(){var $this=$(this);if($this.attr('action')!=''){return;}
                            var action=$this.find('a.action');if(action.length==0){return;}
                            $this.find("input[type=text]").keypress(function(e){if(e.which==13){$this.submit();e.preventDefault();}});$this.submit(function(){$this.find('a.action').eq(0).click();return false;});});function transferLeadership(){function ajaxCall(){var url='https://s144-en.ogame.gameforge.com/game/index.php?page=allianceManagement&action=17';$.post(url,$('#form_setNewLeader').serialize(),function(){window.location.href=getRedirectLink();})}
                            errorBoxDecision('Caution','Are you sure you want to pass on your alliance?','yes','No',ajaxCall);}
                        function dissolve(){function ajaxCall(){var token=$('.dissolve').attr('token');var url='https://s144-en.ogame.gameforge.com/game/index.php?page=allianceManagement&action=2';$.post(url,{token:token},function(){window.location.href=getRedirectLink();})}
                            errorBoxDecision('Caution','Really delete alliance?','yes','No',ajaxCall);}
                        function leaveAlly(){function ajaxCall(){ajaxFormSubmit("leaveAllyForm","https:\/\/s144-en.ogame.gameforge.com\/game\/index.php?page=allianceOverview&action=18",function(){window.location.href=getRedirectLink();});}
                            errorBoxDecision('Caution','Are you sure you want to leave the alliance ?','yes','No',ajaxCall);}
                        function takeoverLeadership(){function ajaxCall(){var token=$('.takeoverLeadership').attr('token');var url='https://s144-en.ogame.gameforge.com/game/index.php?page=allianceManagement&action=11';$.post(url,{token:token},function(data){$('#eins').html(data);})}
                            errorBoxDecision('Caution','Are you sure that you want to take over this alliance?','yes','No',ajaxCall);}
                        $('.kickMemberButton').click(function(){$('#kickMemberReasonText').val("");var data=$(this).attr('id').split('-');var id=data[1];$('#kickMemberId').val(id);});$('#kickMemberForm .cancel').click(function(){$('#kickMemberReason').dialog('destroy');});$('#kickMemberForm').submit(function(e){e.preventDefault();$('#kickMemberReason').dialog('destroy');ajaxFormSubmit("kickMemberForm","https:\/\/s144-en.ogame.gameforge.com\/game\/index.php?page=allianceOverview&action=10",function(data){$("#eins").html(data);});});function deleteRank(){var data=$(this).attr('id').split('-');var id=data[1];var token=$(this).attr('token');function ajaxCall(){var url='https://s144-en.ogame.gameforge.com/game/index.php?page=allianceManagement&action=8&ajax=1';$.post(url,{rankId:id,token:token},function(data){$('#eins').html(data);});}
                            errorBoxDecision('Caution','Do you really want to delete this rank?','yes','No',ajaxCall);}
                        $(document).ready(function(){$('#applicationTab').removeClass('undermark');$('.newApplications').html('');$('.zebra tr').mouseover(function(){$(this).addClass("over");}).mouseout(function(){$(this).removeClass("over");});$('.zebra tr:odd').addClass("alt");$('.section a[rel=\'allyInternText\']').click(function(){var $this=$(this);if(!$this.attr('data-isLoading')){$this.attr('data-isLoading',true);$('#allyInternText #allypage').load("https:\/\/s144-en.ogame.gameforge.com\/game\/index.php?page=allianceOverview&action=20&ajax=1");}});$('.section a[rel=\'allyExternText\']').click(function(){var $this=$(this);if(!$this.attr('data-isLoading')){$this.attr('data-isLoading',true);$('#allyExternText #allypage').load("https:\/\/s144-en.ogame.gameforge.com\/game\/index.php?page=allianceOverview&action=21&ajax=1");}});initBBCodeEditor({"bold":"Bold","italic":"Italic","underline":"Underline","stroke":"Strikethrough","sub":"Subscript","sup":"Superscript","fontColor":"Font colour","fontSize":"Font size","backgroundColor":"Background colour","backgroundImage":"Background image","tooltip":"Tool-tip","alignLeft":"Left align","alignCenter":"Centre align","alignRight":"Right align","alignJustify":"Justify","block":"Break","code":"Code","spoiler":"Spoiler","moreopts":"More Options","list":"List","hr":"Horizontal line","picture":"Image","link":"Link","email":"Email","player":"Player","item":"Item","coordinates":"Coordinates","preview":"Preview","textPlaceHolder":"Text...","playerPlaceHolder":"Player ID or name","itemPlaceHolder":"Item ID","coordinatePlaceHolder":"Galaxy:system:position","charsLeft":"Characters remaining","colorPicker":{"ok":"Ok","cancel":"Cancel","rgbR":"R","rgbG":"G","rgbB":"B"},"backgroundImagePicker":{"ok":"Ok","repeatX":"Repeat horizontally","repeatY":"Repeat vertically"}},{"090a969b05d1b5dc458a6b1080da7ba08b84ec7f":"Bronze Crystal Booster","e254352ac599de4dd1f20f0719df0a070c623ca8":"Bronze Deuterium Booster","b956c46faa8e4e5d8775701c69dbfbf53309b279":"Bronze Metal Booster","3c9f85221807b8d593fa5276cdf7af9913c4a35d":"Bronze Crystal Booster","422db99aac4ec594d483d8ef7faadc5d40d6f7d3":"Silver Crystal Booster","118d34e685b5d1472267696d1010a393a59aed03":"Gold Crystal Booster","d3d541ecc23e4daa0c698e44c32f04afd2037d84":"DETROID Bronze","0968999df2fe956aa4a07aea74921f860af7d97f":"DETROID Gold","27cbcd52f16693023cb966e5026d8a1efbbfc0f9":"DETROID Silver","d9fa5f359e80ff4f4c97545d07c66dbadab1d1be":"Bronze Deuterium Booster","e4b78acddfa6fd0234bcb814b676271898b0dbb3":"Silver Deuterium Booster","5560a1580a0330e8aadf05cb5bfe6bc3200406e2":"Gold Deuterium Booster","40f6c78e11be01ad3389b7dccd6ab8efa9347f3c":"KRAKEN Bronze","929d5e15709cc51a4500de4499e19763c879f7f7":"KRAKEN Gold","4a58d4978bbe24e3efb3b0248e21b3b4b1bfbd8a":"KRAKEN Silver","de922af379061263a56d7204d1c395cefcfb7d75":"Bronze Metal Booster","ba85cc2b8a5d986bbfba6954e2164ef71af95d4a":"Silver Metal Booster","05294270032e5dc968672425ab5611998c409166":"Gold Metal Booster","be67e009a5894f19bbf3b0c9d9b072d49040a2cc":"Bronze Moon Fields","05ee9654bd11a261f1ff0e5d0e49121b5e7e4401":"Gold Moon Fields","c21ff33ba8f0a7eadb6b7d1135763366f0c4b8bf":"Silver Moon Fields","485a6d5624d9de836d3eb52b181b13423f795770":"Bronze M.O.O.N.S.","45d6660308689c65d97f3c27327b0b31f880ae75":"Gold M.O.O.N.S.","fd895a5c9fd978b9c5c7b65158099773ba0eccef":"Silver M.O.O.N.S.","da4a2a1bb9afd410be07bc9736d87f1c8059e66d":"NEWTRON Bronze","8a4f9e8309e1078f7f5ced47d558d30ae15b4a1b":"NEWTRON Gold","d26f4dab76fdc5296e3ebec11a1e1d2558c713ea":"NEWTRON Silver","16768164989dffd819a373613b5e1a52e226a5b0":"Bronze Planet Fields","04e58444d6d0beb57b3e998edc34c60f8318825a":"Gold Planet Fields","0e41524dc46225dca21c9119f2fb735fd7ea5cb3":"Silver Planet Fields"},true,'.alliancetexts',2000);$('.alliancetexts').keyup();});</script><script>
                        initFormValidation();
                    </script></div>
                <div class="new_footer"></div>

            </div>         </div>     </div>

@endsection
