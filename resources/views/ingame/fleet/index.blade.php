@extends('ingame.layouts.main')

@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div id="eventboxContent" style="display: none">
        <img height="16" width="16" src="https://gf3.geo.gfsrv.net/cdne3/3f9884806436537bdec305aa26fc60.gif">
    </div>

    <div id="inhalt" onkeypress="return submitOnEnter(event);">

        <div id="planet" class="">
            <h2>Fleet Dispatch I - Homeworld</h2>
            <a id="toggleHeader" class="toggleHeader" href="javascript:void(0);"><img alt="" src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" height="22" width="22"></a>
        </div>                <div class="fleetStatus">
            <div id="slots" class="fleft">
                <div class="fleft">
                    <span class="tooltip advice" title="Used/Total fleet slots"><span>Fleets                            :</span> 0/1</span>
                </div>
                <div class="fleft">
            <span class="tooltip advice" title="Used/Total expedition slots">
                <span>Expeditions:</span> 0/0                </span>
                </div>
                <br class="clearfloat">
            </div>                        <br class="clearfloat">
        </div>


        <div class="fleetStatus">
            <div id="slots" class="fleft">
                <div class="fleft tactical_retreat">
                    <a title="Tactical retreat|Fleets are able to automatically retreat if they are attacked by a superior force five times stronger than themselves. The crucial factor in this are the attacker`s fleet points in comparison to your fleet points. Defence facilities are not considered.<br /><br />
Civil ships only count 25%, solar satellites and espionage probes are not considered. <br /><br />
Select the option *never* if you would like to deactivate the automatic retreat.<br /><br />
Held fleets are in principle not able to retreat. Death Stars, Espionage Probes and Solar Satellites are also unable to retreat.<br /><br />
Use the Admiral to enable your fleets to retreat from forces three times bigger than your own.<br /><br />
The `tactical retreat` option ends with 500.000 points." href="javascript:void(0);" class="tooltipHTML tooltipRight help"></a>
                    <form class="fleft" name="tacticalRetreat" method="POST" action="">
                    <span class="tooltipHTML tooltipRight" title="Tactical retreat|Fleets are able to automatically retreat if they are attacked by a superior force five times stronger than themselves. The crucial factor in this are the attacker`s fleet points in comparison to your fleet points. Defence facilities are not considered.<br /><br />
Civil ships only count 25%, solar satellites and espionage probes are not considered. <br /><br />
Select the option *never* if you would like to deactivate the automatic retreat.<br /><br />
Held fleets are in principle not able to retreat. Death Stars, Espionage Probes and Solar Satellites are also unable to retreat.<br /><br />
Use the Admiral to enable your fleets to retreat from forces three times bigger than your own.<br /><br />
The `tactical retreat` option ends with 500.000 points.">
                        Tactical retreat:
                    </span>
                        <input onclick="ajaxFormSubmit('tacticalRetreat', 'https://s144-en.ogame.gameforge.com/game/index.php?page=tacticalRetreat&amp;tacticalRetreatState=0');" type="radio" name="tacticalRetreat" value="0"> Never
                        <input onclick="ajaxFormSubmit('tacticalRetreat', 'https://s144-en.ogame.gameforge.com/game/index.php?page=tacticalRetreat&amp;tacticalRetreatState=5');" checked="checked" type="radio" name="tacticalRetreat" value="5"> 5:1

                        <input type="radio" disabled="disabled" name="tacticalRetreat">

                        <a href="https://s144-en.ogame.gameforge.com/game/index.php?page=premium&amp;openDetail=3" class="disabled tooltipHTML" title="Tactical retreat|Use the Admiral to enable your fleets to retreat from forces three times bigger than your own.">
                            3:1
                        </a>
                    </form>
                </div>
                <div class="fleft tooltip" title="Show Deuterium usage per tactical retreat">
                <span>
                    Deuterium consumption:
                </span>

                    0
                </div>
                <br class="clearfloat">
            </div>                <br class="clearfloat">


        </div>

        <div class="c-left"></div>
        <div class="c-right"></div>
        <div id="warning">
            <h3>Fleet dispatch impossible</h3>
            <p>
                <span class="icon icon_warning"></span>
                There are no ships on this planet.                </p>
        </div>
    </div>

@endsection
