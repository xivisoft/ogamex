@extends('ingame.layouts.main')

@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <!-- JAVASCRIPT -->
    <script type="text/javascript">
        var textContent = [];
        textContent[0] = "Diameter:";
        textContent[1] = "{!! $planet_diameter !!}km (<span>0<\/span>\/<span>188<\/span>)";
        textContent[2] = "Temperature:";
        textContent[3] = "{!! $planet_temp_min !!}\u00b0C to {!! $planet_temp_max !!}\u00b0C";
        textContent[4] = "Position:";
        textContent[5] = "<a  href=\"https:\/\/s144-en.ogame.gameforge.com\/game\/index.php?page=galaxy&galaxy=4&system=358&position=4\" >[{!! $planet_coordinates !!}]<\/a>";
        textContent[6] = "Points:";
        textContent[7] = "<a href='https:\/\/s144-en.ogame.gameforge.com\/game\/index.php?page=highscore'>0 (Place {!! $user_rank !!} of {!! $max_rank !!})<\/a>";
        textContent[8] = "Honour points:";
        textContent[9] = "0";

        var textDestination = [];
        textDestination[0] = "diameterField";
        textDestination[1] = "diameterContentField";
        textDestination[2] = "temperatureField";
        textDestination[3] = "temperatureContentField";
        textDestination[4] = "positionField";
        textDestination[5] = "positionContentField";
        textDestination[6] = "scoreField";
        textDestination[7] = "scoreContentField";
        textDestination[8] = "honorField";
        textDestination[9] = "honorContentField";
        var currentIndex = 0;
        var currentChar = 0;
        var linetwo = 0;


        function initType() {
            type();
        }

        $(document).ready(function() {
            initType();
            gfSlider = new GFSlider(getElementByIdWithCache('detailWrapper'));
        });
    </script>

    <div id="eventboxContent" style="display: none">
        <img height="16" width="16" src="https://gf3.geo.gfsrv.net/cdne3/3f9884806436537bdec305aa26fc60.gif" />
    </div>

    <div id="inhalt">
        <div id="planet" style="background-image:url(https://gf1.geo.gfsrv.net/cdn0f/070f90a0f8affc9e89afeb16d5551d.jpg);">

            <div id="detailWrapper">
                <div id="header_text">
                    <h2>
                        <a href="javascript:void(0);" class="openPlanetRenameGiveupBox">

                            <p class="planetNameOverview">Overview -</p>
                        <span id="planetNameHeader">
                            {{ $planet_name }}
                        </span>
                            <img class="hinted tooltip" title="Abandon/Rename Planet" src="https://gf3.geo.gfsrv.net/cdne7/1f57d944fff38ee51d49c027f574ef.gif" width="16" height="16"/>
                        </a>
                    </h2>
                </div>
                <div id="detail" class="detail_screen">
                    <div id="techDetailLoading"></div>
                </div>
                <div id="planetdata">
                    <div class="overlay"></div>
                    <div id="planetDetails">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td class="desc" >
                                    <span id="diameterField"></span>
                                </td>
                                <td class="data">
                                    <span id="diameterContentField"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="desc">
                                    <span id="temperatureField"></span>
                                </td>
                                <td class="data">
                                    <span id="temperatureContentField"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="desc">
                                    <span id="positionField"></span>
                                </td>
                                <td class="data">
                                    <span id="positionContentField"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="desc">
                                    <span id="scoreField"></span></td>
                                <td class="data">
                                    <span id="scoreContentField"></span>
                                </td>
                            </tr>

                            <tr>
                                <td class="desc">
                                    <span id="honorField"></span></td>
                                <td class="data ">
                                    <span id="honorContentField"></span>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div id="planetOptions">

                        <div class="planetMoveStart fleft" style="display: inline;">
                            <a  class="tooltipLeft dark_highlight_tablet fleft"
                                href='https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy'
                                title="The relocation allows you to move your planets to another position in a distant system of your choosing.<br /><br />
The actual relocation first takes place 24 hours after activation. In this time, you can use your planets as normal. A countdown shows you how much time remains prior to the relocation.<br /><br />
Once the countdown has run down and the planet is to be moved, none of your fleets that are stationed there can be active. At this time, there should also be nothing in construction, nothing being repaired and nothing researched. If there is a construction task, a repair task or a fleet still active upon the countdown`s expiry, the relocation will be cancelled.<br /><br />
If the relocation is successful, you will be charged 240.000 Dark Matter. The planets, the buildings and the stored resources including moon will be moved immediately. Your fleets travel to the new coordinates automatically with the speed of the slowest ship. The jump gate to a relocated moon is deactivated for 24 hours."
                                data-tooltip-button="To galaxy">
                                <span class="planetMoveIcons settings planetMoveDefault icon fleft"></span>
                                <span class="planetMoveOverviewMoveLink">Relocate</span>
                            </a>

                        </div>

                        <a class="dark_highlight_tablet float_right openPlanetRenameGiveupBox" href="javascript:void(0);">
                            <span class="planetMoveOverviewGivUpLink">Abandon/Rename</span>
                            <span class="planetMoveIcons settings planetMoveGiveUp icon"></span>
                        </a>
                    </div>
                </div>
            </div>

            <div id="buffBar" class="sliderWrapper">
                <div data-uuid="" data-id="" class="add_item">
                    <a class="activate_item border3px" href="javascript:void(0);"ref="1"></a>
                </div>

                <ul class="active_items hidden">
                    <li>
                    </li>
                </ul>
            </div>



        </div>    <div class="c-left"></div>
        <div class="c-right"></div>
        <div id="overviewBottom">

            <div class="content-box-s">
                <div class="header">
                    <h3>Buildings</h3>
                </div>
                <div class="content">
                    <table cellpadding="0" cellspacing="0" class="construction active">
                        <tr>
                            <td colspan="2" class="idle">
                                <a class="tooltip js_hideTipOnMobile
                               "
                                   title="At the moment there is no building being built on this planet. Click here to get to resources."
                                   href="{{ route('resources.index') }}">
                                    No buildings in construction.<br/>(To resources)                            </a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="footer"></div>
            </div>

            <div class="content-box-s">
                <div class="header"><h3>Research</h3></div>
                <div class="content">
                    <table cellspacing="0" cellpadding="0" class="construction active">
                        <tbody>
                        <tr>
                            <td colspan="2" class="idle">
                                <a class="tooltip js_hideTipOnMobile
                               "
                                   title="There is no research done at the moment. Click here to get to your research lab."
                                   href="{{ route('research.index') }}">
                                    There is no research in progress at the moment.<br/>(To research)                            </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="footer"></div>
            </div>
            <div class="content-box-s">
                <div class="header"><h3>Shipyard</h3></div>
                <div class="content">
                    <table cellspacing="0" cellpadding="0" class="construction active">
                        <tbody>
                        <tr>
                            <td colspan="2" class="idle">
                                <a class="tooltip js_hideTipOnMobile
                           "
                                   title="At the moment there are no ships or defence being built on this planet. Click here to get to the shipyard."
                                   href="{{ route('shipyard.index') }}">
                                    No ships/defence in construction.<br/>(To shipyard)                        </a>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="footer"></div>
            </div>
            <div class="clearfloat"></div>

            <div class="clearfloat"></div>
        </div><!-- #overviewBottom -->
    </div>
@endsection
