@extends('ingame.layouts.main')

@section('content')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div id="inhalt">
        <div id="galaxyHeader">
            <form action="https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy" name="galaform" method="post">
                <span class="galaxy_icons galaxy tooltip" title="Galaxy"></span>
                <span class="galaxy_icons prev" onclick="submitOnKey(40);"></span>
                <input id="galaxy_input" class="hideNumberSpin" maxlength="3" type="text" pattern="[0-9]*" value="4" name="galaxy" tabindex="2" onfocus="clearInput(this);" onkeyup="checkIntInput(this, 1, 6)" onkeypress="return submitOnEnter(event);">
                <span class="galaxy_icons next" onclick="submitOnKey(38);"></span>
                <span class="galaxy_icons solarsystem tooltip" title="System"></span>
                <span class="galaxy_icons prev" onclick="submitOnKey(37);"></span>
                <input id="system_input" class="hideNumberSpin" maxlength="3" type="text" pattern="[0-9]*" value="358" tabindex="2" name="system" onfocus="clearInput(this);" onkeyup="checkIntInput(this, 1, 499)" onkeypress="return submitOnEnter(event);">
                <span class="galaxy_icons next" onclick="submitOnKey(39);"></span>
                <div class="btn_blue" onclick="submitForm();">
                    Go!                </div>
                <div id="expeditionbutton" class="btn_blue float_right" onclick="doExpedition();">
                    Expedition                </div>
            </form>
        </div>
        <div id="eventboxContent" style="display: none">
            <img height="16" width="16" src="https://gf3.geo.gfsrv.net/cdne3/3f9884806436537bdec305aa26fc60.gif">
        </div>
        <div id="galaxyLoading" style="display: none;">
            <img src="https://gf1.geo.gfsrv.net/cdn0e/6e0f46d7504242302bc8055ad9c8c2.gif" alt="">
        </div>
        <div id="galaxyContent"><!--[if lte IE 11]>
            <style type="text/css">
                .icon.icon_eye.hueRotate {
                    background: url(/cdn/img/icons/iconsprite16px.png);
                    background-position: -993px;
                }
            </style>
            <![endif]-->
            <div id="mobileDiv">
                <table cellpadding="0" cellspacing="0" id="galaxytable" border="0" data-galaxy="4" data-system="358">
                    <thead>
                    <tr class="info info_header ct_head_row">
                        <th colspan="11">
                    <span id="probes">
                        Esp.Probe:
                        <span id="probeValue">0</span>
                    </span>
                    <span id="recycler">
                        Recy.:
                        <span id="recyclerValue">0</span>
                    </span>
                    <span id="rockets">
                        IPM.:
                        <span id="missileValue">0</span>
                    </span>
                    <span id="slots">
                        Used slots:
                        <span id="slotValue">
                            <span id="slotUsed">0</span>/1
                        </span>
                    </span>

                    <span class="fright">
                        <span id="filter_empty" class="filter " onclick="filterToggle(event);">E</span>
                        <span id="filter_inactive" class="filter " onclick="filterToggle(event);">I</span>
                        <span id="filter_newbie" class="filter " onclick="filterToggle(event);">N</span>
                        <span id="filter_strong" class="filter " onclick="filterToggle(event);">A</span>
                        <span id="filter_vacation" class="filter " onclick="filterToggle(event);">V</span>
                    </span>
                        </th>
                    </tr>
                    <tr id="galaxyheadbg2" class="ct_head_row">
                        <th class="first" style="width: 70px; overflow: hidden;">Planet</th>
                        <th style="width: 129px; padding-right: 5px;">Name</th>
                        <th class="text_moon" style="width: 38px; padding-right: 5px;">Moon</th>
                        <th style="width: 38px; padding-right: 5px;">DF</th>
                        <th style="width: 130px; padding-right: 5px;">Player (status)</th>
                        <th style="width: 108px; padding-right: 5px;">Alliance</th>
                        <th class="last" style="width: 75px;">Action</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr class="footer ct_foot_row" id="fleetstatus">
                        <td class="ct_foot_row" colspan="11" id="fleetstatusrow">
                        </td>
                    </tr>
                    <tr class="info ct_foot_row">
                        <td colspan="11">
                    <span id="legend">
                        <a href="javascript: void(0);" class="tooltipRel tooltipClose" rel="legendTT">
                            <span class="icon icon_info"></span>
                        </a>
                    </span>
                            <span id="colonized">3 Planets colonised</span>
                            <br class="clearfloat">
                        </td>
                    </tr>
                    </tfoot>
                    <tbody>
                    <tr class="row empty_filter
                                            ">
                        <td class="position js_no_action">1</td>
                        <td colspan="1" class="microplanet planetEmpty js_planet1 js_planetEmpty1">
                            <div id="ownFleetStatus_1_1" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="planetname1 planetEmpty js_planetNameEmpty1" align="center">
                            <span class="tooltip planetMoveIcons colonize-inactive icon" title="It is not possible to colonise a planet without a colony ship."></span>
                            <a class="planetMoveIcons planetMoveDefault tooltip icon js_hideTipOnMobile" href="javascript: void(0);" onclick="movePlanet(
                                       'https://s144-en.ogame.gameforge.com/game/index.php?page=planetMove&amp;action=prepareMove&amp;galaxy=4&amp;system=358&amp;ajax=1&amp;position=1',
                                       'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                                   ); return false;" title="Relocate"></a>
                        </td>

                        <td class="moon js_moon1 js_no_action">
                            <div id="ownFleetStatus_1_3" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="debris js_debris1 ">
                            <div id="ownFleetStatus_1_2" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="playername
                               js_playerName1
                               js_no_action                                                               ">
                                                                                                                <span class="">
                                                                </span>
                                                    <span class="status">
                                                        </span>
                        </td>
                        <td class="allytag
                               js_allyTag1
                               js_no_action                                                               ">
                        </td>
                        <td class="action" colspan="2">
                        <span>
                                                        </span>
                        </td>
                    </tr>
                    <tr class="row empty_filter
                                            ">
                        <td class="position js_no_action">2</td>
                        <td colspan="1" class="microplanet planetEmpty js_planet2 js_planetEmpty2">
                            <div id="ownFleetStatus_2_1" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="planetname1 planetEmpty js_planetNameEmpty2" align="center">
                            <span class="tooltip planetMoveIcons colonize-inactive icon" title="It is not possible to colonise a planet without a colony ship."></span>
                            <a class="planetMoveIcons planetMoveDefault tooltip icon js_hideTipOnMobile" href="javascript: void(0);" onclick="movePlanet(
                                       'https://s144-en.ogame.gameforge.com/game/index.php?page=planetMove&amp;action=prepareMove&amp;galaxy=4&amp;system=358&amp;ajax=1&amp;position=2',
                                       'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                                   ); return false;" title="Relocate"></a>
                        </td>

                        <td class="moon js_moon2 js_no_action">
                            <div id="ownFleetStatus_2_3" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="debris js_debris2 ">
                            <div id="ownFleetStatus_2_2" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="playername
                               js_playerName2
                               js_no_action                                                               ">
                                                                                                                <span class="">
                                                                </span>
                                                    <span class="status">
                                                        </span>
                        </td>
                        <td class="allytag
                               js_allyTag2
                               js_no_action                                                               ">
                        </td>
                        <td class="action" colspan="2">
                        <span>
                                                        </span>
                        </td>
                    </tr>
                    <tr class="row empty_filter
                                            ">
                        <td class="position js_no_action">3</td>
                        <td colspan="1" class="microplanet planetEmpty js_planet3 js_planetEmpty3">
                            <div id="ownFleetStatus_3_1" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="planetname1 planetEmpty js_planetNameEmpty3" align="center">
                            <span class="tooltip planetMoveIcons colonize-inactive icon" title="It is not possible to colonise a planet without a colony ship."></span>
                            <a class="planetMoveIcons planetMoveDefault tooltip icon js_hideTipOnMobile" href="javascript: void(0);" onclick="movePlanet(
                                       'https://s144-en.ogame.gameforge.com/game/index.php?page=planetMove&amp;action=prepareMove&amp;galaxy=4&amp;system=358&amp;ajax=1&amp;position=3',
                                       'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                                   ); return false;" title="Relocate"></a>
                        </td>

                        <td class="moon js_moon3 js_no_action">
                            <div id="ownFleetStatus_3_3" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="debris js_debris3 ">
                            <div id="ownFleetStatus_3_2" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="playername
                               js_playerName3
                               js_no_action                                                               ">
                                                                                                                <span class="">
                                                                </span>
                                                    <span class="status">
                                                        </span>
                        </td>
                        <td class="allytag
                               js_allyTag3
                               js_no_action                                                               ">
                        </td>
                        <td class="action" colspan="2">
                        <span>
                                                        </span>
                        </td>
                    </tr>
                    <tr class="row

                    ">
                        <td class="position js_no_action ">4</td>
                        <td rel="planet4" class="tooltipRel
                                   tooltipClose
                                   tooltipRight
                                   js_hideTipOnMobile
                                   microplanet
                                   js_planet4
                                   colonized
                                   " data-planet-id="33734581" colspan="1">
                            <div class="ListImage">
                                <a href="javascript: void(0);" onclick="return false;">
                                    <img class="planetTooltip dry_3" src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" alt="" height="33" width="38">
                                </a>
                                <div class="activity minute15 tooltip js_hideTipOnMobile" title="Activity">
                                    <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12">
                                </div>
                            </div>
                            <div id="ownFleetStatus_4_1" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                            <div id="planet4" style="display: none;" class="htmlTooltip galaxyTooltip">
                                <h1>Planet: <span class="textNormal">Homeworld</span></h1>
                                <div class="splitLine"></div>
                                <ul class="ListImage">
                                    <li><span id="pos-planet">[4:358:4]</span></li>
                                    <li><img class="planetTooltip dry_3" src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" alt="" height="33" width="38"></li>
                                </ul>
                                <ul class="ListLinks">
                                    <li>Activity:<div class="alert_triangle"><img src="https://gf2.geo.gfsrv.net/cdn12/b4c8503dd1f37dc9924909d28f3b26.gif"></div></li>No actions available.
                                </ul>
                            </div>
                        </td>
                        <td class="planetname ">
                            Homeworld
                        </td>

                        <td class="moon js_moon4 js_no_action">
                            <div id="ownFleetStatus_4_3" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="debris js_debris4 ">
                            <div id="ownFleetStatus_4_2" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="playername
                               js_playerName4
                               js_no_action
                                                           ">
                                                                                                                <span class="status_abbr_active">
                                                                        Commodore N...
                                                                </span>
                                                    <span class="status">

                                                        </span>
                        </td>
                        <td class="allytag
                               js_allyTag4
                                                                                                           status_abbr_buddy
                                                           ">
                        </td>
                        <td class="action" colspan="2">
                        <span>
                                                        </span>
                        </td>
                    </tr>
                    <tr class="row empty_filter
                                            ">
                        <td class="position js_no_action">5</td>
                        <td colspan="1" class="microplanet planetEmpty js_planet5 js_planetEmpty5">
                            <div id="ownFleetStatus_5_1" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="planetname1 planetEmpty js_planetNameEmpty5" align="center">
                            <span class="tooltip planetMoveIcons colonize-inactive icon" title="It is not possible to colonise a planet without a colony ship."></span>
                            <a class="planetMoveIcons planetMoveDefault tooltip icon js_hideTipOnMobile" href="javascript: void(0);" onclick="movePlanet(
                                       'https://s144-en.ogame.gameforge.com/game/index.php?page=planetMove&amp;action=prepareMove&amp;galaxy=4&amp;system=358&amp;ajax=1&amp;position=5',
                                       'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                                   ); return false;" title="Relocate"></a>
                        </td>

                        <td class="moon js_moon5 js_no_action">
                            <div id="ownFleetStatus_5_3" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="debris js_debris5 ">
                            <div id="ownFleetStatus_5_2" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="playername
                               js_playerName5
                               js_no_action                                                               ">
                                                                                                                <span class="">
                                                                </span>
                                                    <span class="status">
                                                        </span>
                        </td>
                        <td class="allytag
                               js_allyTag5
                               js_no_action                                                               ">
                        </td>
                        <td class="action" colspan="2">
                        <span>
                                                        </span>
                        </td>
                    </tr>
                    <tr class="row strong_filter

                    ">
                        <td class="position js_no_action ">6</td>
                        <td rel="planet6" class="tooltipRel
                                   tooltipClose
                                   tooltipRight
                                   js_hideTipOnMobile
                                   microplanet
                                   js_planet6
                                   colonized
                                   " data-planet-id="33734582" colspan="1">
                            <div class="ListImage">
                                <a href="javascript: void(0);" onclick="sendShips(
                                                6,
                                                4,
                                                358,
                                                6,
                                                1,
                                                1
                                                        ); return false;">
                                    <img class="planetTooltip normal_5" src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" alt="" height="33" width="38">
                                </a>

                            </div>
                            <div id="ownFleetStatus_6_1" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                            <div id="planet6" style="display: none;" class="htmlTooltip galaxyTooltip">
                                <h1>Planet: <span class="textNormal">Homeworld</span></h1>
                                <div class="splitLine"></div>
                                <ul class="ListImage">
                                    <li><span id="pos-planet">[4:358:6]</span></li>
                                    <li><img class="planetTooltip normal_5" src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" alt="" height="33" width="38"></li>
                                </ul>
                                <ul class="ListLinks">
                                    <li><a href="javascript:void(0);" onclick="sendShips(6,4,358,6,1,1);return false">Espionage</a></li><li><a href="https://s144-en.ogame.gameforge.com/game/index.php?page=fleet1&amp;galaxy=4&amp;system=358&amp;position=6&amp;type=1&amp;mission=1">Attack</a></li><li><a href="https://s144-en.ogame.gameforge.com/game/index.php?page=fleet1&amp;galaxy=4&amp;system=358&amp;position=6&amp;type=1&amp;mission=3">Transport</a></li>
                                </ul>
                            </div>
                        </td>
                        <td class="planetname ">
                            Homeworld
                        </td>

                        <td class="moon js_moon6 js_no_action">
                            <div id="ownFleetStatus_6_3" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="debris js_debris6 ">
                            <div id="ownFleetStatus_6_2" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="playername
                               js_playerName6
                                                                                                                                            honorableTarget
                                                           ">
                            <a href="javascript: void(0);" class="tooltipRel tooltipClose tooltipRight js_hideTipOnMobile" rel="player108131">
                                <span class="status_abbr_honorableTarget">Proconsul ...</span>
                            </a>
                                                    <span class="status">
                                                                (<span class="status_abbr_honorableTarget"><span class="status_abbr_honorableTarget tooltipHTML" title="Honourable target|In battle against this target you can receive honour points and plunder 50% more loot.">hp</span></span>)
                                                        </span>
                            <div id="player108131" style="display: none;" class="htmlTooltip galaxyTooltip">
                                <h1>Player: <span>Proconsul Nemesis</span></h1>
                                <div class="splitLine"></div>
                                <ul class="ListLinks">
                                    <li class="rank">Ranking: <a href="https://s144-en.ogame.gameforge.com/game/index.php?page=highscore&amp;site=8&amp;searchRelId=108131">790</a></li>
                                    <li><a href="javascript:void(0)" class="sendMail js_openChat tooltip" data-playerid="108131">Write message</a></li>
                                    <li><a href="https://s144-en.ogame.gameforge.com/game/index.php?page=buddies&amp;action=7&amp;id=108131&amp;ajax=1" class="overlay" data-overlay-title="Buddy request to player">Buddy request</a></li>
                                    <li><a href="https://s144-en.ogame.gameforge.com/game/index.php?page=ignorelist&amp;action=1&amp;id=108131">Ignore player</a></li>
                                </ul>
                            </div>
                        </td>
                        <td class="allytag
                               js_allyTag6
                                                                                              ">
                        </td>
                        <td class="action" colspan="2">
                        <span>
                                                                                                                                                                                                                                                        <a class="tooltip js_hideTipOnMobile espionage" title="Espionage" href="javascript: void(0);" onclick="sendShips(
                                                       6,
                                                       4,
                                                       358,
                                                       6,
                                                       1,
                                                       1
                                                   ); return false;">
                                                    <span class="icon icon_eye"></span>
                                                </a>
                                                                                                                                                                                                                                                                                            <a href="javascript:void(0)" class="sendMail js_openChat tooltip" data-playerid="108131" title="Write message"><span class="icon icon_chat"></span></a>
                                                                                                                                                                                                                                                <a class="tooltip overlay buddyrequest" title="Buddy request" href="https://s144-en.ogame.gameforge.com/game/index.php?page=buddies&amp;action=7&amp;id=108131&amp;ajax=1" data-overlay-title="Buddy request to player">
                                                <span class="icon icon_user"></span>
                                            </a>
                                                                                                                                                                                                                                                                        <span class="tooltip js_hideTipOnMobile overlay missleattack" title="Missile Attack" data-overlay-modal="true">
                                                    <span class="icon icon_missile grayscale"></span>
                                                </span>

                                                                                                                                                                                                                                </span>
                        </td>
                    </tr>
                    <tr class="row empty_filter
                                            ">
                        <td class="position js_no_action">7</td>
                        <td colspan="1" class="microplanet planetEmpty js_planet7 js_planetEmpty7">
                            <div id="ownFleetStatus_7_1" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="planetname1 planetEmpty js_planetNameEmpty7" align="center">
                            <span class="tooltip planetMoveIcons colonize-inactive icon" title="It is not possible to colonise a planet without a colony ship."></span>
                            <a class="planetMoveIcons planetMoveDefault tooltip icon js_hideTipOnMobile" href="javascript: void(0);" onclick="movePlanet(
                                       'https://s144-en.ogame.gameforge.com/game/index.php?page=planetMove&amp;action=prepareMove&amp;galaxy=4&amp;system=358&amp;ajax=1&amp;position=7',
                                       'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                                   ); return false;" title="Relocate"></a>
                        </td>

                        <td class="moon js_moon7 js_no_action">
                            <div id="ownFleetStatus_7_3" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="debris js_debris7 ">
                            <div id="ownFleetStatus_7_2" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="playername
                               js_playerName7
                               js_no_action                                                               ">
                                                                                                                <span class="">
                                                                </span>
                                                    <span class="status">
                                                        </span>
                        </td>
                        <td class="allytag
                               js_allyTag7
                               js_no_action                                                               ">
                        </td>
                        <td class="action" colspan="2">
                        <span>
                                                        </span>
                        </td>
                    </tr>
                    <tr class="row empty_filter
                                            ">
                        <td class="position js_no_action">8</td>
                        <td colspan="1" class="microplanet planetEmpty js_planet8 js_planetEmpty8">
                            <div id="ownFleetStatus_8_1" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="planetname1 planetEmpty js_planetNameEmpty8" align="center">
                            <span class="tooltip planetMoveIcons colonize-inactive icon" title="It is not possible to colonise a planet without a colony ship."></span>
                            <a class="planetMoveIcons planetMoveDefault tooltip icon js_hideTipOnMobile" href="javascript: void(0);" onclick="movePlanet(
                                       'https://s144-en.ogame.gameforge.com/game/index.php?page=planetMove&amp;action=prepareMove&amp;galaxy=4&amp;system=358&amp;ajax=1&amp;position=8',
                                       'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                                   ); return false;" title="Relocate"></a>
                        </td>

                        <td class="moon js_moon8 js_no_action">
                            <div id="ownFleetStatus_8_3" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="debris js_debris8 ">
                            <div id="ownFleetStatus_8_2" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="playername
                               js_playerName8
                               js_no_action                                                               ">
                                                                                                                <span class="">
                                                                </span>
                                                    <span class="status">
                                                        </span>
                        </td>
                        <td class="allytag
                               js_allyTag8
                               js_no_action                                                               ">
                        </td>
                        <td class="action" colspan="2">
                        <span>
                                                        </span>
                        </td>
                    </tr>
                    <tr class="row empty_filter
                                            ">
                        <td class="position js_no_action">9</td>
                        <td colspan="1" class="microplanet planetEmpty js_planet9 js_planetEmpty9">
                            <div id="ownFleetStatus_9_1" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="planetname1 planetEmpty js_planetNameEmpty9" align="center">
                            <span class="tooltip planetMoveIcons colonize-inactive icon" title="It is not possible to colonise a planet without a colony ship."></span>
                            <a class="planetMoveIcons planetMoveDefault tooltip icon js_hideTipOnMobile" href="javascript: void(0);" onclick="movePlanet(
                                       'https://s144-en.ogame.gameforge.com/game/index.php?page=planetMove&amp;action=prepareMove&amp;galaxy=4&amp;system=358&amp;ajax=1&amp;position=9',
                                       'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                                   ); return false;" title="Relocate"></a>
                        </td>

                        <td class="moon js_moon9 js_no_action">
                            <div id="ownFleetStatus_9_3" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="debris js_debris9 ">
                            <div id="ownFleetStatus_9_2" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="playername
                               js_playerName9
                               js_no_action                                                               ">
                                                                                                                <span class="">
                                                                </span>
                                                    <span class="status">
                                                        </span>
                        </td>
                        <td class="allytag
                               js_allyTag9
                               js_no_action                                                               ">
                        </td>
                        <td class="action" colspan="2">
                        <span>
                                                        </span>
                        </td>
                    </tr>
                    <tr class="row empty_filter
                                            ">
                        <td class="position js_no_action">10</td>
                        <td colspan="1" class="microplanet planetEmpty js_planet10 js_planetEmpty10">
                            <div id="ownFleetStatus_10_1" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="planetname1 planetEmpty js_planetNameEmpty10" align="center">
                            <span class="tooltip planetMoveIcons colonize-inactive icon" title="It is not possible to colonise a planet without a colony ship."></span>
                            <a class="planetMoveIcons planetMoveDefault tooltip icon js_hideTipOnMobile" href="javascript: void(0);" onclick="movePlanet(
                                       'https://s144-en.ogame.gameforge.com/game/index.php?page=planetMove&amp;action=prepareMove&amp;galaxy=4&amp;system=358&amp;ajax=1&amp;position=10',
                                       'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                                   ); return false;" title="Relocate"></a>
                        </td>

                        <td class="moon js_moon10 js_no_action">
                            <div id="ownFleetStatus_10_3" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="debris js_debris10 ">
                            <div id="ownFleetStatus_10_2" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="playername
                               js_playerName10
                               js_no_action                                                               ">
                                                                                                                <span class="">
                                                                </span>
                                                    <span class="status">
                                                        </span>
                        </td>
                        <td class="allytag
                               js_allyTag10
                               js_no_action                                                               ">
                        </td>
                        <td class="action" colspan="2">
                        <span>
                                                        </span>
                        </td>
                    </tr>
                    <tr class="row empty_filter
                                            ">
                        <td class="position js_no_action">11</td>
                        <td colspan="1" class="microplanet planetEmpty js_planet11 js_planetEmpty11">
                            <div id="ownFleetStatus_11_1" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="planetname1 planetEmpty js_planetNameEmpty11" align="center">
                            <span class="tooltip planetMoveIcons colonize-inactive icon" title="It is not possible to colonise a planet without a colony ship."></span>
                            <a class="planetMoveIcons planetMoveDefault tooltip icon js_hideTipOnMobile" href="javascript: void(0);" onclick="movePlanet(
                                       'https://s144-en.ogame.gameforge.com/game/index.php?page=planetMove&amp;action=prepareMove&amp;galaxy=4&amp;system=358&amp;ajax=1&amp;position=11',
                                       'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                                   ); return false;" title="Relocate"></a>
                        </td>

                        <td class="moon js_moon11 js_no_action">
                            <div id="ownFleetStatus_11_3" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="debris js_debris11 ">
                            <div id="ownFleetStatus_11_2" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="playername
                               js_playerName11
                               js_no_action                                                               ">
                                                                                                                <span class="">
                                                                </span>
                                                    <span class="status">
                                                        </span>
                        </td>
                        <td class="allytag
                               js_allyTag11
                               js_no_action                                                               ">
                        </td>
                        <td class="action" colspan="2">
                        <span>
                                                        </span>
                        </td>
                    </tr>
                    <tr class="row strong_filter

                    ">
                        <td class="position js_no_action ">12</td>
                        <td rel="planet12" class="tooltipRel
                                   tooltipClose
                                   tooltipRight
                                   js_hideTipOnMobile
                                   microplanet
                                   js_planet12
                                   colonized
                                   " data-planet-id="33734604" colspan="1">
                            <div class="ListImage">
                                <a href="javascript: void(0);" onclick="sendShips(
                                                6,
                                                4,
                                                358,
                                                12,
                                                1,
                                                1
                                                        ); return false;">
                                    <img class="planetTooltip ice_1" src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" alt="" height="33" width="38">
                                </a>
                                <div class="activity minute15 tooltip js_hideTipOnMobile" title="Activity">
                                    <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12">
                                </div>
                            </div>
                            <div id="ownFleetStatus_12_1" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                            <div id="planet12" style="display: none;" class="htmlTooltip galaxyTooltip">
                                <h1>Planet: <span class="textNormal">Homeworld</span></h1>
                                <div class="splitLine"></div>
                                <ul class="ListImage">
                                    <li><span id="pos-planet">[4:358:12]</span></li>
                                    <li><img class="planetTooltip ice_1" src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" alt="" height="33" width="38"></li>
                                </ul>
                                <ul class="ListLinks">
                                    <li>Activity:<div class="alert_triangle"><img src="https://gf2.geo.gfsrv.net/cdn12/b4c8503dd1f37dc9924909d28f3b26.gif"></div></li><li><a href="javascript:void(0);" onclick="sendShips(6,4,358,12,1,1);return false">Espionage</a></li><li><a href="https://s144-en.ogame.gameforge.com/game/index.php?page=fleet1&amp;galaxy=4&amp;system=358&amp;position=12&amp;type=1&amp;mission=1">Attack</a></li><li><a href="https://s144-en.ogame.gameforge.com/game/index.php?page=fleet1&amp;galaxy=4&amp;system=358&amp;position=12&amp;type=1&amp;mission=3">Transport</a></li>
                                </ul>
                            </div>
                        </td>
                        <td class="planetname ">
                            Homeworld
                        </td>

                        <td class="moon js_moon12 js_no_action">
                            <div id="ownFleetStatus_12_3" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="debris js_debris12 ">
                            <div id="ownFleetStatus_12_2" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="playername
                               js_playerName12
                                                                                                                                            honorableTarget
                                                           ">
                            <a href="javascript: void(0);" class="tooltipRel tooltipClose tooltipRight js_hideTipOnMobile" rel="player108132">
                                <span class="status_abbr_honorableTarget">qwerqwerfg</span>
                            </a>
                                                    <span class="status">
                                                                (<span class="status_abbr_honorableTarget"><span class="status_abbr_honorableTarget tooltipHTML" title="Honourable target|In battle against this target you can receive honour points and plunder 50% more loot.">hp</span></span>)
                                                        </span>
                            <div id="player108132" style="display: none;" class="htmlTooltip galaxyTooltip">
                                <h1>Player: <span>qwerqwerfg</span></h1>
                                <div class="splitLine"></div>
                                <ul class="ListLinks">
                                    <li class="rank">Ranking: <a href="https://s144-en.ogame.gameforge.com/game/index.php?page=highscore&amp;site=8&amp;searchRelId=108132">763</a></li>
                                    <li><a href="javascript:void(0)" class="sendMail js_openChat tooltip" data-playerid="108132">Write message</a></li>
                                    <li><a href="https://s144-en.ogame.gameforge.com/game/index.php?page=buddies&amp;action=7&amp;id=108132&amp;ajax=1" class="overlay" data-overlay-title="Buddy request to player">Buddy request</a></li>
                                    <li><a href="https://s144-en.ogame.gameforge.com/game/index.php?page=ignorelist&amp;action=1&amp;id=108132">Ignore player</a></li>
                                </ul>
                            </div>
                        </td>
                        <td class="allytag
                               js_allyTag12
                                                                                              ">
                        </td>
                        <td class="action" colspan="2">
                        <span>
                                                                                                                                                                                                                                                        <a class="tooltip js_hideTipOnMobile espionage" title="Espionage" href="javascript: void(0);" onclick="sendShips(
                                                       6,
                                                       4,
                                                       358,
                                                       12,
                                                       1,
                                                       1
                                                   ); return false;">
                                                    <span class="icon icon_eye"></span>
                                                </a>
                                                                                                                                                                                                                                                                                            <a href="javascript:void(0)" class="sendMail js_openChat tooltip" data-playerid="108132" title="Write message"><span class="icon icon_chat"></span></a>
                                                                                                                                                                                                                                                <a class="tooltip overlay buddyrequest" title="Buddy request" href="https://s144-en.ogame.gameforge.com/game/index.php?page=buddies&amp;action=7&amp;id=108132&amp;ajax=1" data-overlay-title="Buddy request to player">
                                                <span class="icon icon_user"></span>
                                            </a>
                                                                                                                                                                                                                                                                        <span class="tooltip js_hideTipOnMobile overlay missleattack" title="Missile Attack" data-overlay-modal="true">
                                                    <span class="icon icon_missile grayscale"></span>
                                                </span>

                                                                                                                                                                                                                                </span>
                        </td>
                    </tr>
                    <tr class="row empty_filter
                                            ">
                        <td class="position js_no_action">13</td>
                        <td colspan="1" class="microplanet planetEmpty js_planet13 js_planetEmpty13">
                            <div id="ownFleetStatus_13_1" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="planetname1 planetEmpty js_planetNameEmpty13" align="center">
                            <span class="tooltip planetMoveIcons colonize-inactive icon" title="It is not possible to colonise a planet without a colony ship."></span>
                            <a class="planetMoveIcons planetMoveDefault tooltip icon js_hideTipOnMobile" href="javascript: void(0);" onclick="movePlanet(
                                       'https://s144-en.ogame.gameforge.com/game/index.php?page=planetMove&amp;action=prepareMove&amp;galaxy=4&amp;system=358&amp;ajax=1&amp;position=13',
                                       'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                                   ); return false;" title="Relocate"></a>
                        </td>

                        <td class="moon js_moon13 js_no_action">
                            <div id="ownFleetStatus_13_3" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="debris js_debris13 ">
                            <div id="ownFleetStatus_13_2" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="playername
                               js_playerName13
                               js_no_action                                                               ">
                                                                                                                <span class="">
                                                                </span>
                                                    <span class="status">
                                                        </span>
                        </td>
                        <td class="allytag
                               js_allyTag13
                               js_no_action                                                               ">
                        </td>
                        <td class="action" colspan="2">
                        <span>
                                                        </span>
                        </td>
                    </tr>
                    <tr class="row empty_filter
                                            ">
                        <td class="position js_no_action">14</td>
                        <td colspan="1" class="microplanet planetEmpty js_planet14 js_planetEmpty14">
                            <div id="ownFleetStatus_14_1" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="planetname1 planetEmpty js_planetNameEmpty14" align="center">
                            <span class="tooltip planetMoveIcons colonize-inactive icon" title="It is not possible to colonise a planet without a colony ship."></span>
                            <a class="planetMoveIcons planetMoveDefault tooltip icon js_hideTipOnMobile" href="javascript: void(0);" onclick="movePlanet(
                                       'https://s144-en.ogame.gameforge.com/game/index.php?page=planetMove&amp;action=prepareMove&amp;galaxy=4&amp;system=358&amp;ajax=1&amp;position=14',
                                       'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                                   ); return false;" title="Relocate"></a>
                        </td>

                        <td class="moon js_moon14 js_no_action">
                            <div id="ownFleetStatus_14_3" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="debris js_debris14 ">
                            <div id="ownFleetStatus_14_2" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="playername
                               js_playerName14
                               js_no_action                                                               ">
                                                                                                                <span class="">
                                                                </span>
                                                    <span class="status">
                                                        </span>
                        </td>
                        <td class="allytag
                               js_allyTag14
                               js_no_action                                                               ">
                        </td>
                        <td class="action" colspan="2">
                        <span>
                                                        </span>
                        </td>
                    </tr>
                    <tr class="row empty_filter
                                            ">
                        <td class="position js_no_action">15</td>
                        <td colspan="1" class="microplanet planetEmpty js_planet15 js_planetEmpty15">
                            <div id="ownFleetStatus_15_1" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="planetname1 planetEmpty js_planetNameEmpty15" align="center">
                            <span class="tooltip planetMoveIcons colonize-inactive icon" title="It is not possible to colonise a planet without a colony ship."></span>
                            <a class="planetMoveIcons planetMoveDefault tooltip icon js_hideTipOnMobile" href="javascript: void(0);" onclick="movePlanet(
                                       'https://s144-en.ogame.gameforge.com/game/index.php?page=planetMove&amp;action=prepareMove&amp;galaxy=4&amp;system=358&amp;ajax=1&amp;position=15',
                                       'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                                   ); return false;" title="Relocate"></a>
                        </td>

                        <td class="moon js_moon15 js_no_action">
                            <div id="ownFleetStatus_15_3" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="debris js_debris15 ">
                            <div id="ownFleetStatus_15_2" class="fleetAction">
                                <img src="https://gf2.geo.gfsrv.net/cdndf/3e567d6f16d040326c7a0ea29a4f41.gif" width="12" height="12" alt="">
                            </div>
                        </td>
                        <td class="playername
                               js_playerName15
                               js_no_action                                                               ">
                                                                                                                <span class="">
                                                                </span>
                                                    <span class="status">
                                                        </span>
                        </td>
                        <td class="allytag
                               js_allyTag15
                               js_no_action                                                               ">
                        </td>
                        <td class="action" colspan="2">
                        <span>
                                                        </span>
                        </td>
                    </tr>

                    </tbody>
                </table>



                <div id="legendTT" style="display: none;" class="htmlTooltip">
                    <h1>Legend</h1>
                    <div class="splitLine"></div>
                    <dl>
                        <dt class="abbreviation status_abbr_admin">A</dt>
                        <dd class="description">Administrator</dd>

                        <dt class="abbreviation status_abbr_strong">s</dt>
                        <dd class="description">Stronger Player</dd>

                        <dt class="abbreviation status_abbr_noob">n</dt>
                        <dd class="description">Weaker Player (newbie)</dd>

                        <dt class="abbreviation status_abbr_outlaw">o</dt>
                        <dd class="description">Outlaw (temporary)</dd>

                        <dt class="abbreviation status_abbr_vacation">v</dt>
                        <dd class="description">Vacation Mode</dd>

                        <dt class="abbreviation status_abbr_banned">b</dt>
                        <dd class="description">Banned</dd>

                        <dt class="abbreviation status_abbr_inactive">i</dt>
                        <dd class="description">7 days inactive</dd>

                        <dt class="abbreviation status_abbr_longinactive">I</dt>
                        <dd class="description">28 days inactive</dd>

                        <dt class="abbreviation status_abbr_honorableTarget">hp</dt>
                        <dd class="description">Honourable target</dd>
                    </dl>
                </div>
            </div>
            <script type="text/javascript">

                var galaxy = 4;
                var system = 358;

                var buildListCountdowns = new Array();
                $(document).ready(function() {
                    buildListCountdowns.push(
                            new baulisteCountdown(
                                    document.getElementById("cooldown-0"),
                                    0,
                                    'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                            )
                    );
                    buildListCountdowns.push(
                            new baulisteCountdown(
                                    document.getElementById("cooldown-1"),
                                    0,
                                    'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                            )
                    );
                    buildListCountdowns.push(
                            new baulisteCountdown(
                                    document.getElementById("cooldown-2"),
                                    0,
                                    'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                            )
                    );
                    buildListCountdowns.push(
                            new baulisteCountdown(
                                    document.getElementById("cooldown-3"),
                                    0,
                                    'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                            )
                    );
                    buildListCountdowns.push(
                            new baulisteCountdown(
                                    document.getElementById("cooldown-4"),
                                    0,
                                    'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                            )
                    );
                    buildListCountdowns.push(
                            new baulisteCountdown(
                                    document.getElementById("cooldown-5"),
                                    0,
                                    'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                            )
                    );
                    buildListCountdowns.push(
                            new baulisteCountdown(
                                    document.getElementById("cooldown-6"),
                                    0,
                                    'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                            )
                    );
                    buildListCountdowns.push(
                            new baulisteCountdown(
                                    document.getElementById("cooldown-7"),
                                    0,
                                    'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                            )
                    );
                    buildListCountdowns.push(
                            new baulisteCountdown(
                                    document.getElementById("cooldown-8"),
                                    0,
                                    'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                            )
                    );
                    buildListCountdowns.push(
                            new baulisteCountdown(
                                    document.getElementById("cooldown-9"),
                                    0,
                                    'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                            )
                    );
                    buildListCountdowns.push(
                            new baulisteCountdown(
                                    document.getElementById("cooldown-10"),
                                    0,
                                    'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                            )
                    );
                    buildListCountdowns.push(
                            new baulisteCountdown(
                                    document.getElementById("cooldown-11"),
                                    0,
                                    'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                            )
                    );
                    buildListCountdowns.push(
                            new baulisteCountdown(
                                    document.getElementById("cooldown-12"),
                                    0,
                                    'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                            )
                    );
                    buildListCountdowns.push(
                            new baulisteCountdown(
                                    document.getElementById("cooldown-13"),
                                    0,
                                    'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                            )
                    );
                    buildListCountdowns.push(
                            new baulisteCountdown(
                                    document.getElementById("cooldown-14"),
                                    0,
                                    'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                            )
                    );
                    buildListCountdowns.push(
                            new baulisteCountdown(
                                    document.getElementById("cooldown-15"),
                                    0,
                                    'https://s144-en.ogame.gameforge.com/game/index.php?page=galaxy&amp;galaxy=4&amp;system=358'
                            )
                    );

                    $(document.documentElement).off( "keyup" );
                    $(document.documentElement).on( "keyup", keyevent );
                });
            </script>
        </div>
    </div>

@endsection
