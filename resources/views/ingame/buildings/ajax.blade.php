<input type="hidden" name="modus" value="1">
<input type="hidden" name="type" value="{!! $id !!}">
<input type="hidden" name="planet_id" value="{!! $planet_id !!}">



<div id="demolish1" style="display: none;">
    <div class="htmlTooltip">
        <h1>Deconstruction costs:</h1>
        <div class="splitLine"></div>
        <table cellpadding="0" cellspacing="0" class="demolishinfo">
            <tr class="costReduction">
                <td class="res">Ion technology bonus:</td>
                <td class="value undermark">-0%</td>
            </tr>
            <tr>
                <td class="res">Duration:</td>
                <td class="value">2s</td>
            </tr>
        </table>
    </div>    </div>

<div id="{{ $building_type }}_{!! $id !!}_large" class="pic">

    <a href="https://s128-en.ogame.gameforge.com/game/index.php?page=techtree&tab=4&techID=1" class="techtree_link js_hideTipOnMobile tooltip overlay"
       data-overlay-title="Techtree - {!! $title !!}"
       title="No requirements available">
        <div class="techtree_img_disabled"></div>
        <span class="label">Techtree</span>
    </a>


</div>

<div id="content">

    <h2>{!! $title !!}</h2>
    <a id="close" class='close_details'
       href="javascript:void(0);"
       onclick="gfSlider.hide(getElementByIdWithCache('detail')); return false;">
    </a>
        <span class="level">
        Level {!! $current_level !!}        <span class="undermark"></span>
	</span>
    <br class="clearfloat"/>

    @if ($building_type == 'shipyard' || $building_type == 'defense')
        <ul class="production_info narrow">
            <li>
                Production duration            <span class="time" id="buildDuration">
                <a href="https://s137-en.ogame.gameforge.com/game/index.php?page=station" class="tooltip" title="Not able to calculate without a shipyard!">Unknown</a>                <span class="undermark">
                                    </span>
            </span>
            </li>
            <li>
                Construction possible: <span class="time" id="possibleInTime"><span href="javascript:void(0)" class="dark_highlight_tablet tooltip advice" title="Requirements are not met">Unknown</span>            </span></li>
        </ul>
        <div class="enter">
            <p class="amount">Number:</p>
            <div class="clearfix maxlink_wrap">
                <input id="number" type="text" class="amount_input" pattern="[0-9,.]*" size="5" name="amount" value="1" onfocus="clearInput(this);" onkeyup="checkIntInput(this, 1, 9999);event.stopPropagation();">
                <div class="maxlink_arrow"></div>
                <a id="maxlink" class="tooltip js_hideTipOnMobile" title="Produce maximum number" href="javascript:void(0);" onclick="document.forms['form'].amount.value = {{ $max_build_amount }};">
                    [max. {{ $max_build_amount }}]
                </a>
            </div>
        </div>
    @else
    <ul class="production_info ">
        <li>
            Production duration            <span class="time" id="buildDuration">
                {!! $production_time !!}                <span class="undermark">
                                    </span>
            </span>
        </li>
        @if ($energy_difference > 0)
        <li>
            Energy needed:
            <span class="time">{{ $energy_difference }}</span>
        </li>
        @elseif ($energy_difference < 0)
        <li>
            Production:
            <span class="time">{{ $production_next['energy'] }} <span class="undermark"> (+{{ ($energy_difference * -1) }})</span>
        </li>
        @endif
    </ul>
    @endif

    <div class="costs_wrap">

        @if ($building_type == 'shipyard' || $building_type == 'defense')
            <p class="costs_info">Costs per piece:</p>
        @else
            <p class="costs_info">Required to improve to level {!! $next_level !!}:</p>
        @endif

        <ul id="costs">
            @if (!empty($price['metal']))
            <li class="metal tooltip" title="{!! $price_formatted['metal'] !!} Metal">
                <div class="resourceIcon metal"></div>
                <div class="cost @if ($planet->getMetal() < $price['metal'])
                        overmark
                        @endif">
                    {!! $price_formatted['metal'] !!}	                </div>
            </li>
            @endif
            @if (!empty($price['crystal']))
            <li class="crystal tooltip" title="{!! $price_formatted['crystal'] !!} Crystal">
                <div class="resourceIcon crystal"></div>
                <div class="cost @if ($planet->getCrystal() < $price['crystal'])
                        overmark
                        @endif">
                    {!! $price_formatted['crystal'] !!}	                </div>
            </li>
            @endif
            @if (!empty($price['deuterium']))
            <li class="deuterium tooltip" title="{!! $price_formatted['deuterium'] !!} Deuterium">
                <div class="resourceIcon deuterium"></div>
                <div class="cost @if ($planet->getDeuterium() < $price['deuterium'])
                        overmark
                        @endif">
                    {!! $price_formatted['deuterium'] !!}	                </div>
            </li>
            @endif
        </ul>
    </div>

    <div class="build-it_wrap">
        <!-- all ok -->
        <div class="premium_info_placeholder"></div>
        <a class="@if (!$enough_resources || !$requirements_met || $build_queue_max)
                build-it_disabled isWorking
@else
build-it
                @endif"
           href="javascript:void(0);">
            <span>
            @if ($building_type == 'shipyard' || $building_type == 'defense')
            Build
            @elseif (!empty($build_active['id']))
            In queue
            @else
            Improve
            @endif</span>
        </a>

    </div>

</div>
<br clear="all"/>

<div id="description">


    <div class="txt_box  ">
        <a class="tooltip js_hideTipOnMobile help overlay"
           href="https://s128-en.ogame.gameforge.com/game/index.php?page=techtree&tab=2&techID=1"
           title="More details">
        </a>
        <p class="description_txt" style="width: 90%">{!! $description !!}</p>
    </div>
</div>

<script>
    $(document).ready(function() {
        $("#number").focus();
    });

    $(".build-it_disabled:not(.isWorking)")
            .click(function() {
                errorBoxDecision('Error','You need a Commander to be able to use the building queue. Would you like to learn more about the advantages of a Commander?','yes','No', function() { window.location.href = 'https://s128-en.ogame.gameforge.com/game/index.php?page=premium&openDetail=2' });
            });

    var links = {
        'overlay' 	:	'https://s128-en.ogame.gameforge.com/game/index.php?page=buyResourceOverlay&techID=1',
        'decisionCommander' 	:	'https://s128-en.ogame.gameforge.com/game/index.php?page=premium&openDetail=2',
        '1000'	:	'https://s128-en.ogame.gameforge.com/game/index.php?page=premium&openDetail=1',
        '10' : 'https://s128-en.ogame.gameforge.com/game/index.php?page=resources&openTech=22',
        '20' : 'https://s128-en.ogame.gameforge.com/game/index.php?page=resources&openTech=23',
        '30' : 'https://s128-en.ogame.gameforge.com/game/index.php?page=resources&openTech=24',
        'notify'	:	'https://s128-en.ogame.gameforge.com/game/index.php?page=station&openTech=44'
    };

    var loca = loca || {};
    loca = $.extend({},
            loca,
            {
                'allError'				:	'Error',
                'infoBuildlist'			:	'You need a Commander to be able to use the building queue. Would you like to learn more about the advantages of a Commander?',
                'allYes'				:	'yes',
                'allNo'					:	'No',
                'allOk'					:	'Ok',
                'noRocketsiloCapacity'	:	'Not enough capacity. Upgrade missile silo.',
                'allDetailNow'			:	'now'
            }
    );

    var buttonClass = "build-it";

    var overlayTitle = 'Start with DM';

    var showSlotWarning = 1;

    var buttonState = 1;

    var techID = 1;

    var isRocketAndStorageNotFree = 0;

    var couldBeBuild = 1;

    var isShip = 0;

    var isRocket = 0;

    var hasCommander = 0;

    var buildableAt = null;

    var error = 2000;

    var premiumerror = 0;

    var showErrorOnPremiumbutton = 0;

    var errorlist = {
        '2000' : 'With a price of 0 DM the profit margin is too low for the merchant!',
        '100' : 'The merchant can only deliver resources to an amount totalling 10.000.000 to you',
        '10' : 'Not enough storage capacity. - Would you like to expand your storage?',
        '20' : 'Not enough storage capacity. - Would you like to expand your storage?',
        '30' : 'Not enough storage capacity. - Would you like to expand your storage?',
        '1000' : 'Not enough Dark Matter available! Do you want to buy some now?'
    };


    var isBuildlistNeeded = 0;

    //var showCommanderHint = (!buttonState && !hasCommander && isBuildlistNeeded && couldBeBuild && (isShip || isRocket));

    var showNoPremiumError = 0;

    var pageToReload = "{{ route('resources.index') }}";

    var isBusy = 0;

    initTechDetailsAjax();
</script>
